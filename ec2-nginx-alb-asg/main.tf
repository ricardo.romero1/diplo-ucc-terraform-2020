module "ec2-nginx-alb-asg" {
  source               = "../modules/terraform-aws-nginx-asg"
  name                 = var.name
  environment          = var.environment
  ami                  = var.ami
  instance_type        = var.instance_type
  instance_volume_size = var.instance_volume_size
  elb_subnets_ids      = data.terraform_remote_state.vpc.outputs.public_subnets
  asg_subnets_ids      = data.terraform_remote_state.vpc.outputs.private_subnets
  asg_min_size         = var.asg_min_size
  asg_max_size         = var.asg_max_size
  asg_desired_capacity = var.asg_desired_capacity
  vpc_id               = data.terraform_remote_state.vpc.outputs.vpc_id
  health_check_target  = var.health_check_target
  ingress_cidr_blocks  = var.instance_ssh_ingress_cidr_blocks
  key_pair_name        = var.key_pair_name
  year                 = var.year
}