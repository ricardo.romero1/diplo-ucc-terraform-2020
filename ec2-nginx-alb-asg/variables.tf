variable "environment" {
  description = "Environment to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "name" {
  description = "Creates a unique name for autoscaling group of instances"
  type        = string
  default     = null
}

variable "ami" {
  description = "EC2 AMI to be used with instances"
  type        = string
  default     = null
}

variable "instance_type" {
  description = "Instance type"
  type        = string
  default     = null

}

variable "instance_volume_size" {
  description = "EC2 instance root disk size"
  type        = number
  default     = null

}

variable "elb_subnets_ids" {
  description = "A list of subnets to place the Load Balancer"
  type        = list(string)
  default     = null

}

variable "asg_subnets_ids" {
  description = "A list of subnets to place EC2 nginx instances"
  type        = list(string)
  default     = null

}

variable "asg_min_size" {
  description = "Min number of instances"
  type        = number
  default     = null

}

variable "asg_max_size" {
  description = "Max number of instances"
  type        = number
  default     = null

}

variable "asg_desired_capacity" {
  description = "Desired number of instances"
  type        = number
  default     = null

}

variable "vpc_id" {
  description = "VPC where the instances will be deployed.	"
  type        = string
  default     = null

}

variable "instance_ssh_ingress_cidr_blocks" {
  description = "CIDR block from where to ssh into EC2 instances"
  type        = list(string)
  default     = null

}

variable "key_pair_name" {
  description = "key pair for the ec2"
  type        = string
  default     = "vpn"
}

variable "year" {
  description = "Year of the diplo"
  type        = string
}

variable "health_check_target" {
  description = "Protocol, Port and path to check health for NGINX"
  type        = string
  default     = "HTTP:80/"
}