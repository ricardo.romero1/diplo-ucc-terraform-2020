data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}
  
data "aws_region" "current" {}
data "aws_availability_zones" "available" {}
 
module "vpc" {
  source = "../modules/terraform-aws-vpc"
   
  name               = ""
  vpc_name           = var.vpc_name
  cidr               = var.cidr
  azs                = var.azs
  private_subnets    = var.private_subnets
  public_subnets     = var.public_subnets
  database_subnets   = var.database_subnets
  # elasticache_subnets= var.elasticache_subnets
  create_database_subnet_group = false

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_classiclink             = false
  enable_classiclink_dns_support = false

  enable_nat_gateway = true
  single_nat_gateway = true

 
  enable_vpn_gateway = false

  enable_dhcp_options              = false
  dhcp_options_domain_name         = ""
  dhcp_options_domain_name_servers = ["127.0.0.1", "10.0.0.2"]
 
  # VPC endpoint for S3
  # enable_s3_endpoint = true

  # VPC endpoint for DynamoDB
  # enable_dynamodb_endpoint = false
/***
  # VPC endpoint for SSM
  enable_ssm_endpoint              = true
  ssm_endpoint_private_dns_enabled = false
  ssm_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC endpoint for SSMMESSAGES
  enable_ssmmessages_endpoint              = false
  ssmmessages_endpoint_private_dns_enabled = false
  ssmmessages_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for EC2
  enable_ec2_endpoint              = false
  ec2_endpoint_private_dns_enabled = false
  ec2_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for EC2MESSAGES
  enable_ec2messages_endpoint              = false
  ec2messages_endpoint_private_dns_enabled = false
  ec2messages_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for ECR API
  enable_ecr_api_endpoint              = false
  ecr_api_endpoint_private_dns_enabled = false
  ecr_api_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC Endpoint for ECR DKR
  enable_ecr_dkr_endpoint              = false
  ecr_dkr_endpoint_private_dns_enabled = false
  ecr_dkr_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC endpoint for KMS
  enable_kms_endpoint              = false
  kms_endpoint_private_dns_enabled = false
  kms_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC endpoint for ECS
  enable_ecs_endpoint              = false
  ecs_endpoint_private_dns_enabled = false
  ecs_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC endpoint for ECS telemetry
  enable_ecs_telemetry_endpoint              = false
  ecs_telemetry_endpoint_private_dns_enabled = false
  ecs_telemetry_endpoint_security_group_ids  = [data.aws_security_group.default.id]

  # VPC endpoint for SQS
  enable_sqs_endpoint              = false
  sqs_endpoint_private_dns_enabled = false
  sqs_endpoint_security_group_ids  = [data.aws_security_group.default.id]
***/
  # VPC Flow Logs (Cloudwatch log group and IAM role will be created)
  # enable_flow_log                      = true
  # create_flow_log_cloudwatch_log_group = true
  # create_flow_log_cloudwatch_iam_role  = true

  tags = {
    env       = var.env
    Environment = var.environment
  }
  nat_gateway_tags = {
    Name  = "diplo-ucc-2020-nat_gateway"
  } 
  nat_eip_tags = {
    Name = "diplo-ucc-2020-nat-eip"
  }
  igw_tags = {
    Name = "diplo-ucc-2020-production-igw"
  }
  vpc_endpoint_tags = {
    Project  = "diplo-ucc-2020"
    Endpoint = "true"
  }
}
