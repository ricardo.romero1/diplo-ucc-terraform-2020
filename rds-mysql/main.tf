module "mysql" {
  source                              = "../modules/terraform-rds-mysql"
  name                                = var.aurora_name
  engine                              = var.engine
  engine_version                      = var.engine_version
  # subnets                             = data.terraform_remote_state.vpc.outputs.database_subnets
  # vpc_id                              = data.terraform_remote_state.vpc.outputs.vpc_id
  # replica_count                       = var.replica_count
  # instance_type                       = var.aurora_instance_type
  allocated_storage                   = var.allocated_storage
  identifier                          = var.identifier
  instance_class                      = var.instance_class
  maintenance_window                  = var.maintenance_window
  backup_window                       = var.backup_window
  apply_immediately                   = var.apply_immediately
  skip_final_snapshot                 = var.skip_final_snapshot
  # db_parameter_group_name             = aws_db_parameter_group.aurora_db_57_parameter_group.id
  # db_cluster_parameter_group_name     = aws_rds_cluster_parameter_group.aurora_57_cluster_parameter_group.id
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  enabled_cloudwatch_logs_exports     = var.enabled_cloudwatch_logs_exports
  # allowed_cidr_blocks                 = var.allowed_cidr_blocks
  # create_security_group = var.create_security_group
  # database_name = var.database_name
  username = var.username
  password = var.password
  # final_snapshot_identifier_prefix = var.final_snapshot_identifier_prefix
  deletion_protection = var.deletion_protection
  backup_retention_period = var.backup_retention_period
  #preferred_backup_window = var.preferred_backup_window
  #preferred_maintenance_window = var.preferred_maintenance_window
  port = var.aurora_port
  monitoring_interval = var.monitoring_interval
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  #scaling_configuration     = var.scaling_configuration
  snapshot_identifier     = var.snapshot_identifier
  storage_encrypted     = var.storage_encrypted
  kms_key_id     = var.kms_key_id
  #enable_http_endpoint     = var.enable_http_endpoint
  # replica_scale_enabled     = var.replica_scale_enabled
  # replica_scale_max     = var.replica_scale_max
  # replica_scale_min     = var.replica_scale_min
  # replica_scale_cpu     = var.replica_scale_cpu
  # replica_scale_connections     = var.replica_scale_connections
  # replica_scale_in_cooldown     = var.replica_scale_in_cooldown
  # replica_scale_out_cooldown     = var.replica_scale_out_cooldown
  performance_insights_enabled     = var.performance_insights_enabled
  # performance_insights_kms_key_id     = var.performance_insights_kms_key_id
  # global_cluster_identifier     = var.global_cluster_identifier
  # engine_mode     = var.engine_mode
  # replication_source_identifier     = var.replication_source_identifier
  # source_region     = var.source_region
  vpc_security_group_ids     = var.vpc_security_group_ids
  db_subnet_group_name     = var.db_subnet_group_name
  #predefined_metric_type     = var.predefined_metric_type
  #backtrack_window     = var.backtrack_window
  copy_tags_to_snapshot     = var.copy_tags_to_snapshot
  #iam_roles     = var.iam_roles
  #security_group_description     = var.security_group_description
  #permissions_boundary     = var.permissions_boundary
  ca_cert_identifier     = var.ca_cert_identifier
  tags                            = {
            Application = "RDS"
            CreatedBy   = "terraform"
            Environment = "vpn"
            tovalue     = "stop"
        }

}

resource "aws_db_parameter_group" "aurora_db_57_parameter_group" {
  name        = "${var.env}-aurora-db-57-parameter-group"
  family      = "aurora-mysql5.7"
  description = "${var.env}-aurora-db-57-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_57_cluster_parameter_group" {
  name        = "${var.env}-aurora-57-cluster-parameter-group"
  family      = "aurora-mysql5.7"
  description = "${var.env}-aurora-57-cluster-parameter-group"

 parameter {
 name = "binlog_format"
 value = "ROW"
 apply_method = "pending-reboot"
 }
  parameter {
   name = "log_bin_trust_function_creators"
   value  = "1"
   apply_method = "pending-reboot"
 }
  parameter {
   name = "general_log"
   value  = "1"
 }
  parameter {
   name = "slow_query_log"
   value  = "1"
 }
  parameter {
   name = "long_query_time"
   value  = "2"
 }
}


resource "aws_security_group" "app_servers" {
  name_prefix = "app-servers"
  description = "For application servers"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id
}
resource "aws_security_group_rule" "allow_access" {
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.app_servers.id
  security_group_id        = module.mysql.this_security_group_id
}
