terraform {
  backend "s3" {
    bucket = "cloudshine-diplo-ucc-2020-tfstate"
    key    = "rds/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "cloudshine-diplo-ucc-2020-tfstate"
    key    = "vpc/terraform.tfstate"
    region = "us-east-1"
  }
} 