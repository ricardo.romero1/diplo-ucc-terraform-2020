variable "environment" {
  description = "Environment to be used on all the resources as identifier"
  type        = string
  default     = ""
}

variable "name" {
  description = "Creates a unique name for autoscaling group of runners"
  type        = string
  default     = null
}

variable "ami" {
  description = "EC2 AMI to be used with runner instances"
  type        = string
  default     = null
}

variable "instance_type" {
  description = "Instance type"
  type        = string
  default     = null

}

variable "instance_volume_size" {
  description = "EC2 instance root disk size"
  type        = number
  default     = null

}

variable "elb_subnets_ids" {
  description = "A list of subnets to place the Load Balancer"
  type        = list(string)
  default     = null

}

variable "asg_subnets_ids" {
  description = "A list of subnets to place EC2 nginx instances"
  type        = list(string)
  default     = null

}

variable "asg_min_size" {
  description = "Min number of runners"
  type        = number
  default     = null

}

variable "asg_max_size" {
  description = "Max number of runners"
  type        = number
  default     = null

}

variable "asg_desired_capacity" {
  description = "Desired number of runners"
  type        = number
  default     = null

}

variable "vpc_id" {
  description = "VPC where the runners will be deployed.	"
  type        = string
  default     = null

}

variable "instance_ssh_ingress_cidr_blocks" {
  description = "CIDR block from where to ssh into runners EC2 instances"
  type        = list(string)
  default     = null

}

variable "health_check_target" {
  description = "Protocol, Port and path to check health for NGINX"
  type        = string
  default     = "HTTP:80/"

}

variable "ecr_id" {
  description = "ECR for nginx docker image to be pulled"
  type        = string
  default     = null

}

variable "ingress_cidr_blocks" {
  description = "List of IPv4 CIDR ranges to use on all ingress rules"
  type        = list(string)
  default     = []
}

variable "key_pair_name" {
  description = "Name for the keypair for EC2 instances created by EB"
  type        = string
}

variable "tags" {
  description = "tags for infra"
  type        = map(string)
  default     = {}
}

variable "year" {
  description = "Year of the diplo"
  type        = string
}
