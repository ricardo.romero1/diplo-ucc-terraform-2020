locals {
  user_data = <<EOF
#!/bin/bash
sudo apt update
sudo apt install nginx -y
EOF
}

module "asg" {
  source = "github.com/terraform-aws-modules/terraform-aws-autoscaling"

  name = "${var.environment}-${var.year}-nginx-asg-${var.name}"

  # Launch configuration
  lc_name = "nginx-lc"

  image_id        = var.ami
  instance_type   = var.instance_type
  security_groups = [module.asg_sg.this_security_group_id]

  load_balancers       = [module.elb.this_elb_id]
  user_data_base64     = base64encode(local.user_data)
  iam_instance_profile = aws_iam_instance_profile.nginx_instances_profile.id
  key_name             = var.key_pair_name

  root_block_device = [
    {
      volume_size = var.instance_volume_size
      volume_type = "gp2"
    },
  ]

  # Auto scaling group
  asg_name                  = var.name
  vpc_zone_identifier       = var.asg_subnets_ids
  health_check_type         = "EC2"
  min_size                  = var.asg_min_size
  max_size                  = var.asg_max_size
  desired_capacity          = var.asg_desired_capacity
  wait_for_capacity_timeout = 0

  tags = [
    {
      key                 = "Environment"
      value               = var.environment
      propagate_at_launch = true
    },
    {
      key                 = "nginx-${var.environment}"
      value               = "true"
      propagate_at_launch = true
    },
  ]
}

######
# ELB
######
module "elb" {
  source  = "terraform-aws-modules/elb/aws"
  #version = "~> 5.0"
  
  name = "${var.environment}-${var.year}-nginx-alb-${var.name}"

  #load_balancer_type = "application"

  #vpc_id             = var.vpc_id
  subnets            = var.elb_subnets_ids
  security_groups    = [module.elb_sg.this_security_group_id]

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    },
  ]

  health_check = {
    target              = var.health_check_target
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  tags = {
    Name        = "${var.environment}-${var.year}-alb"
    Terraform   = "true"
    Environment = var.environment # "diplo-ucc"
    Year        = var.year #"2020"
  }
}

module "asg_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "nginx-${var.environment}-${var.year}-asg-sg"
  description = "Security group for NGINX EC2 instances"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "all-icmp", "https-443-tcp", "ssh-tcp"]
  egress_rules        = ["all-all"]
  ingress_with_cidr_blocks = [
    {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      description = "mysql port"
      cidr_blocks = "0.0.0.0/16"
    },
    {
      rule        = "postgresql-tcp"
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}

module "elb_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name        = "nginx-${var.environment}-${var.year}-elb-sg"
  description = "Security group for NGINX ELB"
  vpc_id      = var.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp"]
  egress_rules        = ["all-all"]
}

resource "aws_iam_instance_profile" "nginx_instances_profile" {
  name = "nginx_${var.environment}_instances_profile"
  role = aws_iam_role.nginx_role.name
}

resource "aws_iam_role" "nginx_role" {
  name = "nginx-${var.environment}-role"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
